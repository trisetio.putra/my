from django.urls import path,include
from . import views

app_name= 'homepage'

urlpatterns=[
    path('',views.index,name='index'),
    path('profile/',views.profile,name='profile'),
    path('page2/',views.page2,name='page2'),
    path('page3/',views.page3,name='page3'),
    path('page4/',views.page4,name='page4'),
    path('page5/',views.page5,name='page5'),
    path('navbar/',views.navbar, name='navbar'),

]
