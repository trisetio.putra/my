from django.shortcuts import render

# Create your views here.
def index(request):
    return render(request,'mainpage.html')

def profile(request):
    return render(request,'Story1.html')

def page2(request):
    return render(request,'page2.html')

def page3(request):
    return render(request,'page3.html')

def page4(request):
    return render(request,'page4.html')

def page5(request):
    return render(request,'page5.html')

def navbar(request):
    return render(request,'navbar.html')
